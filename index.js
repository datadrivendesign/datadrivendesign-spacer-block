module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Spacer Block',
  contextualOnly: true,
  addFields: [
    {
      type: 'area',
      name: 'center',
      label: 'Center',
      contextual: true
    }
  ],
  construct: function(self, options) {
    var superPushAssets = self.pushAssets;
    self.pushAssets = function() {
      superPushAssets();
      self.pushAsset('stylesheet', 'spacer-block', { when: 'always' });
    }
  }
};
