# README #

### What is this repository for? ###

* Adds a block to the editor allowing the user to create a bootstrap 12 column layout with the class datadrivendesign-spacer-block added to allow the easy definition of user addable spacer blocks in the CSS.
* 1.0.0

### How do I get set up? ###


```
#!bash

npm install -save git@bitbucket.org:datadrivendesign/datadrivendesign-spacer-block.git
```


### How to use it? ###

```
#!javascript

{{ apos.area(data.page, 'area-name', {
    widgets: {
       'datadrivendesign-spacer-block': {}
    }
}) }}
```